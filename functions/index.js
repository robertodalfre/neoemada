'use strict'

const functions = require('firebase-functions')
const admin = require('firebase-admin')
admin.initializeApp()

function deleteCollection(db, collectionPath, batchSize, filter = { field: '', value: '' }) {
  const { field, value } = filter
  let collectionRef = db.collectionGroup(collectionPath)
  let query = collectionRef.where(field, '==', value ).orderBy('__name__').limit(batchSize)

  return new Promise((resolve, reject) => {
    deleteQueryBatch(db, query, batchSize, resolve, reject)
  })
}

function deleteQueryBatch(db, query, batchSize, resolve, reject) {
  query.get()
    .then((snapshot) => {
      // When there are no documents left, we are done
      if (snapshot.size === 0) {
        return 0
      }

      // Delete documents in a batch
      let batch = db.batch()
      snapshot.docs.forEach((doc) => {
        batch.delete(doc.ref)
      })

      return batch.commit().then(() => {
        return snapshot.size
      })
    }).then((numDeleted) => {
      if (numDeleted === 0) {
        resolve()
        return
      }

      // Recurse on the next process tick, to avoid
      // exploding the stack.
      process.nextTick(() => {
        deleteQueryBatch(db, query, batchSize, resolve, reject)
      })
    })
    .catch(reject)
}

// Send Notification when User Emitted new Nfse
exports.sendAdminNotification = functions.firestore.document('/users/{userId}/clients/{clientId}/nfes/{nfeId}')
  .onCreate(async (snap, context) => {
    // Grab the current value of what was written to Cloud Firestore.
    const { userId } = snap.data()

    // Access the parameter `{documentId}` with `context.params`
    functions.logger.log('SendNotification new nfe', snap.data())

    // Get the list of device notification admin tokens.
    const getAdminDeviceTokensPromise = admin.firestore()
      .collectionGroup(`notificationTokens`).where('role', '==', 'admin').get()

    // Get the user profile.
    const getUserProfilePromise = admin.auth().getUser(userId)

    // The snapshot to the user's tokens.
    let tokensSnapshot

    // The array containing all the admins's tokens.
    let tokens

    const results = await Promise.all([getAdminDeviceTokensPromise, getUserProfilePromise])
    tokensSnapshot = results[0]
    const user = results[1]

    tokensSnapshot = tokensSnapshot.docs.map(doc => {
      return {
        _id: doc.id,
        ...doc.data()
      }
    })

    // Check if there are any device tokens.
    if (tokensSnapshot.length <= 0) {
      return console.log('There are no notification tokens to send to.')
    }
    console.log('There are', tokensSnapshot.length, 'tokens to send notifications to.')
    console.log('Fetched user profile', user)

    // Listing all tokens as an array.
    tokens = tokensSnapshot.map(cur => cur.notificationToken)

    // Notification details.
    const payload = {
      webpush: {
        notification: {
          title: 'Solicitação de Nova Nota!',
          body: `${user.displayName || user.email } acabou de emitir uma nota`,
          icon: user.photoURL || ''
        },
        fcm_options: {
          link: 'https://notanamao-52131.web.app/#/dashboard/nfe'
        }
      },
      android: {
        ttl: 3600 * 1000,
        priority: "normal",
        notification: {
          title: "Solicitação de Nova Nota!",
          body: `${user.displayName || user.email } acabou de emitir uma nota`,
          icon: user.photoURL || 'stock_ticker_update',
          color: "#f45342",
          visibility: "public",
          click_action: "https://notanamao-52131.web.app/#/dashboard/nfe"
        }
      },
      apns: {
        payload: {
          aps: {
            badge: 42,
            category: 'https://notanamao-52131.web.app/#/dashboard/nfe'
          }
        }
      },
      tokens
    }
    // const payload = {
    //   notification: {
    //     title: 'Solicitação de Nova Nota!',
    //     body: `${user.displayName || user.email } acabou de emitir uma nota`,
    //     icon: user.photoURL || ''
    //   }
    // }

    // Send notifications to all tokens.
    // const response = await admin.messaging().sendToDevice(tokens, payload)
    const response = await admin.messaging().sendMulticast(payload)

    // For each message check if there was an error.
    const tokensToRemove = []
    if (response.failureCount > 0) {
      response.responses.forEach((resp, idx) => {
        if (!resp.success) {
          console.error('Failure sending notification to', tokens[idx])
          tokensToRemove.push(deleteCollection(admin.firestore(), 'notificationTokens', 5, { field: 'notificationToken', value: tokensSnapshot[idx].notificationToken } ))
        }
      })
    }
    // // For each message check if there was an error.
    // let tokensToRemove = []
    // response.results.forEach(async (result, index) => {
    //   const error = result.error
    //   if (error) {
    //     console.error('Failure sending notification to', tokens[index], error)
    //     // Cleanup the tokens who are not registered anymore.
    //     if (error.code === 'messaging/invalid-registration-token' ||
    //       error.code === 'messaging/registration-token-not-registered') {
    //       tokensToRemove.push(deleteCollection(admin.firestore(), 'notificationTokens', 5, { field: 'notificationToken', value: tokensSnapshot[index].notificationToken } ))
    //     }
    //   }
    // })
    return Promise.all(tokensToRemove)
  })

// Send Notification when NFSe emitted by admin or register completed.
exports.sendUserNotification = functions.https.onCall(async (data, context) => {

  // Grab the current value of what was send by client.
  const { userId, typeMessage, callId } = data

  // Access the data parameter
  functions.logger.log('SendNotification', data)

  // Get the list of device notification tokens.
  const getDeviceTokensPromise = admin.firestore()
    .collection('users').doc(userId).collection(`notificationTokens`).get()

    // Get the user profile.
    const getUserProfilePromise = admin.auth().getUser(userId)

    // The snapshot to the user's tokens.
    let tokensSnapshot

    // The array containing all the users's tokens.
    let tokens

    const results = await Promise.all([getDeviceTokensPromise, getUserProfilePromise])
    tokensSnapshot = results[0]
    const user = results[1]

    tokensSnapshot = tokensSnapshot.docs.map(doc => {
      return {
        _id: doc.id,
        ...doc.data()
      }
    })

    // Check if there are any device tokens.
    if (tokensSnapshot.length <= 0) {
      return console.log('There are no notification tokens to send to.')
    }
    console.log('There are', tokensSnapshot.length, 'tokens to send notifications to.')
    console.log('Fetched user profile', user)

    // Listing all tokens as an array.
    tokens = tokensSnapshot.map(cur => cur.notificationToken)
    let payload = {}

    // Notification details.
    if (typeMessage === 0) {
      payload = {
        webpush: {
          notification: {
            title: 'Nota Emitida!',
            body: `${user.displayName || user.email } sua nota acabou de ser emitida!`,
            icon: user.photoURL || 'stock_ticker_update'
          },
          fcm_options: {
            link: 'https://notanamao-52131.web.app'
          }
        },
        android: {
          ttl: 3600 * 1000,
          priority: "normal",
          notification: {
            title: 'Nota Emitida!',
            body: `${user.displayName || user.email } sua nota acabou de ser emitida!`,
            icon: user.photoURL || 'stock_ticker_update',
            color: "#f45342",
            visibility: "public",
            click_action: "https://notanamao-52131.web.app/#/"
          }
        },
        apns: {
          payload: {
            aps: {
              badge: 42,
              category: 'https://notanamao-52131.web.app/#/'
            }
          }
        },
        tokens
      }
    } else if (typeMessage === 1) {
      payload = {
        webpush: {
          notification: {
            title: 'Cadastro Aprovado!',
            body: `${user.displayName || user.email} você já pode emitir sua primeira NFSe!`,
            icon: user.photoURL || 'stock_ticker_update'
          },
          fcm_options: {
            link: 'https://notanamao-52131.web.app/#/'
          }
        },
        android: {
          ttl: 3600 * 1000,
          priority: "normal",
          notification: {
            title: 'Cadastro Aprovado!',
            body: `${user.displayName || user.email} você já pode emitir sua primeira NFSe!`,
            icon: user.photoURL || 'stock_ticker_update',
            color: "#f45342",
            visibility: "public",
            click_action: "https://notanamao-52131.web.app/#/"
          }
        },
        apns: {
          payload: {
            aps: {
              badge: 42,
              category: 'https://notanamao-52131.web.app/#/'
            }
          }
        },
        tokens
      }
    } else if (typeMessage === 2) {
      payload = {
        webpush: {
          notification: {
            title: 'Nova Mensagem!',
            body: `${user.displayName || user.email} você recebeu uma nova mensagem!`,
            icon: user.photoURL || 'stock_ticker_update'
          },
          fcm_options: {
            link: `https://notanamao-52131.web.app/#/mensagens/${callId}?userId=${userId}`
          }
        },
        android: {
          ttl: 3600 * 1000,
          priority: "normal",
          notification: {
            title: 'Nova Mensagem!',
            body: `${user.displayName || user.email} você recebeu uma nova mensagem!`,
            icon: user.photoURL || 'stock_ticker_update',
            color: "#f45342",
            visibility: "public",
            click_action: `https://notanamao-52131.web.app/#/mensagens/${callId}?userId=${userId}`
          }
        },
        apns: {
          payload: {
            aps: {
              badge: 42,
              category: `https://notanamao-52131.web.app/#/mensagens/${callId}?userId=${userId}`
            }
          }
        },
        tokens
      }
    }
    // Send notifications to all tokens.
    const response = await admin.messaging().sendMulticast(payload)

    // For each message check if there was an error.
    const tokensToRemove = []
    if (response.failureCount > 0) {
      response.responses.forEach((resp, idx) => {
        if (!resp.success) {
          console.error('Failure sending notification to', tokens[idx])
          tokensToRemove.push(deleteCollection(admin.firestore(), 'notificationTokens', 5, { field: 'notificationToken', value: tokensSnapshot[idx].notificationToken } ))
        }
      })
      await Promise.all(tokensToRemove)
    }
    console.log(response.successCount + ' messages were sent successfully')
    return response
})

// Send Notification when User completed your register
exports.sendNotification = functions.https.onCall(async (data, context) => {

  // Grab the current value of what was send by client.
  const { userId, typeMessage, callId } = data

  // Access the data parameter
  functions.logger.log('SendNotification', data)

  // Get the list of device notification admin tokens.
  const getAdminDeviceTokensPromise = admin.firestore()
    .collectionGroup(`notificationTokens`).where('role', '==', 'admin').get()

    // Get the user profile.
    const getUserProfilePromise = admin.auth().getUser(userId)

    // The snapshot to the admin's tokens.
    let tokensSnapshot

    // The array containing all the admins's tokens.
    let tokens

    const results = await Promise.all([getAdminDeviceTokensPromise, getUserProfilePromise])
    tokensSnapshot = results[0]
    const user = results[1]

    tokensSnapshot = tokensSnapshot.docs.map(doc => {
      return {
        _id: doc.id,
        ...doc.data()
      }
    })

    // Check if there are any device tokens.
    if (tokensSnapshot.length <= 0) {
      return console.log('There are no notification tokens to send to.')
    }
    console.log('There are', tokensSnapshot.length, 'tokens to send notifications to.')
    console.log('Fetched user profile', user)

    // Listing all tokens as an array.
    tokens = tokensSnapshot.map(cur => cur.notificationToken)

    let payload = {}
    // Notification details.
    if (typeMessage === 0 ) {
      payload = {
        webpush: {
          notification: {
            title: 'Novo Cadastro!',
            body: `${user.displayName || user.email } está solicitando aprovação do seu cadastro`,
            icon: user.photoURL || 'stock_ticker_update'
          },
          fcm_options: {
            link: 'https://notanamao-52131.web.app/#/dashboard/users'
          }
        },
        android: {
          ttl: 3600 * 1000,
          priority: "normal",
          notification: {
            title: 'Novo Cadastro!',
            body: `${user.displayName || user.email } está solicitando aprovação do seu cadastro`,
            icon: user.photoURL || 'stock_ticker_update',
            color: "#f45342",
            visibility: "public",
            click_action: "https://notanamao-52131.web.app/#/dashboard/users"
          }
        },
        apns: {
          payload: {
            aps: {
              badge: 42,
              category: 'https://notanamao-52131.web.app/#/dashboard/users'
            }
          }
        },
        tokens
      }
    } else if (typeMessage === 2) {
      payload = {
        webpush: {
          notification: {
            title: 'Nova Mensagem!',
            body: `${user.displayName || user.email} mandou uma mensagem!`,
            icon: user.photoURL || 'stock_ticker_update'
          },
          fcm_options: {
            link: `https://notanamao-52131.web.app/#/dashboard/chamados/${callId}?userId=${userId}`
          }
        },
        android: {
          ttl: 3600 * 1000,
          priority: "normal",
          notification: {
            title: 'Nova Mensagem!',
            body: `${user.displayName || user.email} mandou uma mensagem!`,
            icon: user.photoURL || 'stock_ticker_update',
            color: "#f45342",
            visibility: "public",
            click_action: `https://notanamao-52131.web.app/#/dashboard/chamados/${callId}?userId=${userId}`
          }
        },
        apns: {
          payload: {
            aps: {
              badge: 42,
              category: `https://notanamao-52131.web.app/#/dashboard/chamados/${callId}?userId=${userId}`
            }
          }
        },
        tokens
      }
    }
    // Send notifications to all tokens.
    const response = await admin.messaging().sendMulticast(payload)

    // For each message check if there was an error.
    const tokensToRemove = []
    if (response.failureCount > 0) {
      response.responses.forEach((resp, idx) => {
        if (!resp.success) {
          console.error('Failure sending notification to', tokens[idx])
          tokensToRemove.push(deleteCollection(admin.firestore(), 'notificationTokens', 5, { field: 'notificationToken', value: tokensSnapshot[idx].notificationToken } ))
        }
      })
      await Promise.all(tokensToRemove)
    }
    console.log(response.successCount + ' messages were sent successfully')
    return response
})
