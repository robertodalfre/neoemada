import toString from 'lodash/toString'
export const rules = {
  computed: {
    isRequired (val) {
      return [val => !!val || 'Campo Obrigatório']
    },
    isValidCardNumber (val) {
      return [val => {
        const brandCard = {
          Visa: /^4[0-9]{12}(?:[0-9]{3})/,
          Mastercard: /^5[1-5][0-9]{14}/,
          Amex: /^3[47][0-9]{13}/,
          DinersClub: /^3(?:0[0-5]|[68][0-9])[0-9]{11}/,
          Discover: /^6(?:011|5[0-9]{2})[0-9]{12}/,
          JCB: /^(?:2131|1800|35\d{3})\d{11}/
        }

        const number = val.split(' ').join('')

        if (isNaN(number)) return 'Número do cartão não é válido'

        let valid = false
        for (var brand in brandCard) {
          if (number.match(brandCard[brand])) {
            valid = true
          }
        }

        if (!valid) return 'Número do cartão não é válido'

        return null
      }]
    },
    isValidPhoneNumber (val) {
      return [val => {
        if (!val) return 'Campo Obrigatório'

        const number = val.replace(/\(|\)|\s|-/g, '')

        if (isNaN(number) || (number.length !== 11 && number.length !== 10)) return 'Número do telefone não é válido'

        return null
      }]
    },
    validDate (val) {
      const regexString = '^(?:(?:31(\\/|-|\\.)(?:0?[13578]|1[02]))\\1|(?:(?:29|30)(\\/|-|\\.)(?:0?[1,3-9]|1[0-2])\\2))(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$|^(?:29(\\/|-|\\.)0?2\\3(?:(?:(?:1[6-9]|[2-9]\\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\\d|2[0-8])(\\/|-|\\.)(?:(?:0?[1-9])|(?:1[0-2]))\\4(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$'
      const regex = new RegExp(regexString)
      return [val => regex.test(val) || 'Data Inválida']
    },
    isValidPasswordLength (val) {
      return [
        val => !!val || 'Campo Obrigatório',
        val => val.length >= 6 || 'Senha não pode ter menos que 6 caracteres'
      ]
    },
    isValidEmailFormat (val) {
      return [
        val => !!val || 'Campo Obrigatório',
        val => val && (/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(val) || 'Informe um e-mail válido')
      ]
    },
    isValidEmailFormatOptional (val) {
      return [
        val => !!val || null,
        val => val && (/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(val) || 'Informe um e-mail válido')
      ]
    },
    isValidName (val) {
      return [val => {
        if (!val || toString(val) === '') return 'Campo Obrigatório'
        const re = /([a-zA-Z\s])/g

        if ((toString(val) || '').match(re).length !== (val || '').length) return 'Não é permitido números e/ou caracteres especiais'

        return null
      }]
    },
    isCNPJ (val) {
      return [val => {
        if (!val || toString(val) === '') return 'Campo Obrigatório'

        const valClear = val.replace(/[^\d]+/g, '')

        if (valClear.length !== 14) return 'CNPJ Inválido'

        if (valClear === '00000000000000' ||
          valClear === '11111111111111' ||
          valClear === '22222222222222' ||
          valClear === '33333333333333' ||
          valClear === '44444444444444' ||
          valClear === '55555555555555' ||
          valClear === '66666666666666' ||
          valClear === '77777777777777' ||
          valClear === '88888888888888' ||
          valClear === '99999999999999') return 'CNPJ Inválido'

        let size = valClear.length - 2
        let partIni = valClear.substring(0, size)
        const dig = valClear.substring(size)
        let sum = 0
        let pos = size - 7

        for (let i = size; i >= 1; i--) {
          sum += partIni.charAt(size - i) * pos--
          if (pos < 2) pos = 9
        }
        let result = sum % 11 < 2 ? 0 : 11 - sum % 11
        if (toString(result) !== toString(dig.charAt(0))) return 'CNPJ Inválido'

        size = size + 1
        partIni = valClear.substring(0, size)
        sum = 0
        pos = size - 7
        for (let i = size; i >= 1; i--) {
          sum += partIni.charAt(size - i) * pos--
          if (pos < 2) pos = 9
        }
        result = sum % 11 < 2 ? 0 : 11 - sum % 11
        if (toString(result) !== toString(dig.charAt(1))) return 'CNPJ Inválido'

        return null
      }]
    }
  }
}
