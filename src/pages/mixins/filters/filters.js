import { date } from 'quasar'
export const filters = {
  filters: {
    capitalize (value) {
      if (!value) return ''
      value = value.toString()
      return value.charAt(0).toUpperCase() + value.slice(1)
    },
    formatPrazo (value) {
      if (!value) return ''
      value = value.toString()
      if (value === '1') return `${value} dia útil`
      if (value === '0') return 'Hoje'
      return `${value} dias úteis`
    },
    currency (value) {
      if (!value) return ''
      return `R$ ${parseFloat(value).toFixed(2)}`
    },
    round (value) {
      if (!value) return (0).toFixed(2).replace('.', ',')
      return `${parseFloat(value).toFixed(2).replace('.', ',')}`
    },
    formatDate (value) {
      if (!value) return ''
      return date.formatDate(value, 'DD/MM/YYYY')
    },
    formatHour (value) {
      if (!value) return ''
      return date.formatDate(value, 'HH:MM')
    },
    translate (value) {
      if (!value) return ''
      return value === 'Administrador' ? 'Administrador' : ''
    }

  }
}
