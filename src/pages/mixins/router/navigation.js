export const navigation = {
  methods: {
    goBack () {
      this.$router.go(-1)
    },
    hideForm () {
      this.$router.push({ name: `${this.routerName}.index` })
    },
    edit (payload) {
      const id = payload.id
      const query = payload.query
      this.$router.push({ name: `${this.routerName}.edit`, params: { id }, query: { query } })
    },
    create () {
      this.$router.push({ name: `${this.routerName}.new` })
    },
    view (payload) {
      const id = payload.id
      this.$router.push({ name: `${this.routerName}.view`, params: { id } })
    },
    hideMain () {
      this.$router.push({ name: 'dashboard.index' })
    }
  },
  computed: {
    isView () {
      return this.$route.name === `${this.routerName}.view`
    },
    isCreating () {
      return this.$route.name === `${this.routerName}.new`
    },
    isEdit () {
      return this.$route.name === `${this.routerName}.edit`
    }
  }
}
