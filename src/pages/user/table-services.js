export const columns = [
  {
    name: 'activityId',
    label: 'Código',
    field: 'activityId',
    width: '100px',
    style: { color: 'dark' },
    classes: 'bg-white',
    filter: true,
    sortable: false,
    type: 'string',
    align: 'left'
  },
  {
    name: 'description',
    label: 'Descrição',
    field: 'description',
    width: '100px',
    style: { color: 'dark' },
    classes: 'bg-white',
    filter: true,
    sortable: false,
    type: 'string',
    align: 'left'
  }
]
export const visibleColumns = ['activityId', 'description']
