export const columns = [
  {
    name: 'userId',
    label: 'userId',
    field: 'userId',
    width: '100px',
    style: { color: 'dark' },
    classes: 'bg-white',
    headerClasses: 'bg-grey-2 text-grey-9',
    filter: true,
    sortable: true,
    type: 'string',
    align: 'left'
  },
  {
    name: 'status',
    label: 'Status',
    field: 'status',
    classes: 'bg-white',
    options: [
      { label: 'Cadastro Pendente', value: 0, color: 'blue-4' },
      { label: 'Aguardando Aprovação', value: 1, color: 'yellow-9' },
      { label: 'Cadastro Completo', value: 2, color: 'green-5' }
    ],
    mapOptions: {
      0: 'blue-4',
      1: 'yellow-9',
      2: 'green-5'
    },
    mapOptionsIcons: {
      0: 'fas fa-stopwatch',
      1: 'fas fa-spinner',
      2: 'fas fa-check'
    },
    headerClasses: 'bg-grey-2 text-grey-9',
    filter: true,
    sortable: true,
    editable: true,
    type: 'select',
    align: 'left'
  },
  {
    name: 'email',
    label: 'email',
    field: 'email',
    width: '100px',
    style: { color: 'dark' },
    classes: 'bg-white',
    headerClasses: 'bg-grey-2 text-grey-9',
    filter: true,
    sortable: true,
    type: 'string',
    align: 'left'
  },
  {
    name: 'fullname',
    label: 'Nome',
    field: 'fullname',
    width: '100px',
    style: { color: 'dark' },
    classes: 'bg-white',
    headerClasses: 'bg-grey-2 text-grey-9',
    filter: true,
    sortable: true,
    type: 'string',
    align: 'left'
  },
  {
    name: 'loginPref',
    label: 'Login Pref.',
    field: 'loginPref',
    classes: 'bg-white',
    headerClasses: 'bg-grey-2 text-grey-9',
    filter: true,
    sortable: true,
    type: 'string',
    align: 'left'
  },
  {
    name: 'passPref',
    label: 'Senha Pref.',
    field: 'passPref',
    classes: 'bg-white',
    headerClasses: 'bg-grey-2 text-grey-9',
    filter: true,
    sortable: true,
    type: 'string',
    align: 'left'
  },
  {
    name: 'phoneNumber',
    label: 'Telefones',
    field: (value) => value.phoneNumber1 && value.phoneNumber2
      ? `${value.phoneNumber1.replace(/^(\d{2})(\d)/g, '($1) $2').replace(/(\d)(\d{4})$/, '$1-$2') || ''} /
      ${value.phoneNumber2.replace(/^(\d{2})(\d)/g, '($1) $2').replace(/(\d)(\d{4})$/, '$1-$2') || ''}`
      : '',
    width: '100px',
    style: { color: 'dark' },
    classes: 'bg-white',
    headerClasses: 'bg-grey-2 text-grey-9',
    filter: true,
    sortable: true,
    type: 'string',
    align: 'left'
  },
  {
    name: 'address.city',
    label: 'Endereço',
    field: value => value.address && value.address.city ? `${value.address.city || ''} - ${value.address.state || ''} ` : '',
    width: '100px',
    style: { color: 'dark' },
    classes: 'bg-white',
    headerClasses: 'bg-grey-2 text-grey-9',
    filter: true,
    sortable: true,
    type: 'string',
    align: 'left'
  },
  {
    name: 'role',
    label: 'Perfil',
    field: 'role',
    width: '100px',
    style: { color: 'dark' },
    classes: 'bg-white',
    headerClasses: 'bg-grey-2 text-grey-9',
    filter: true,
    sortable: true,
    type: 'select',
    align: 'left',
    options: [
      { label: 'Administrador', value: 'admin' },
      { label: 'Usuário', value: 'user' }
    ],
    format: value => value === 'admin' ? 'Administrador' : 'Usuário'
  },
  {
    name: 'createdAt',
    label: 'Data de criação',
    field: 'createdAt',
    width: '100px',
    style: { color: 'dark' },
    classes: 'bg-white',
    headerClasses: 'bg-grey-2 text-grey-9',
    filter: true,
    sortable: true,
    type: 'string',
    align: 'left'
  }
]
export const visibleColumns = ['fullname', 'email', 'loginPref', 'passPref', 'phoneNumber', 'address.city', 'role', 'status', 'createdAt']
