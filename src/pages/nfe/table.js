import { date } from 'quasar'
export const columns = [
  {
    name: 'number',
    label: 'Número NFSe',
    field: 'number',
    width: '100px',
    classes: 'bg-grey-2',
    headerClasses: 'bg-grey-2 text-grey-9',
    filter: true,
    sortable: true,
    type: 'string',
    editable: true,
    align: 'left'
  },
  {
    name: 'status',
    label: 'Status',
    field: 'status',
    classes: 'bg-white',
    options: [
      { label: 'Aguardando Emissão', value: 0, color: 'blue-4' },
      { label: 'Emitida', value: 1, color: 'green-5' },
      { label: 'Emissão em Andamento', value: 2, color: 'yellow-9' },
      { label: 'Cancelada', value: 3, color: 'red-5' }
    ],
    mapOptions: {
      0: 'blue-4',
      1: 'green-5',
      2: 'yellow-9',
      3: 'red-5'
    },
    mapOptionsIcons: {
      0: 'fas fa-stopwatch',
      1: 'fas fa-check',
      2: 'fas fa-spinner',
      3: 'close'
    },
    headerClasses: 'bg-grey-2 text-grey-9',
    filter: true,
    sortable: true,
    editable: true,
    type: 'select',
    align: 'left'
  },
  {
    name: 'userLoginPref',
    label: 'Login Pref.',
    field: 'userLoginPref',
    classes: 'bg-white',
    headerClasses: 'bg-grey-2 text-grey-9',
    filter: true,
    sortable: true,
    type: 'string',
    align: 'left',
    copyable: true
  },
  {
    name: 'userPassPref',
    label: 'Senha Pref.',
    field: 'userPassPref',
    classes: 'bg-white',
    headerClasses: 'bg-grey-2 text-grey-9',
    filter: true,
    sortable: true,
    type: 'string',
    align: 'left',
    copyable: true
  },
  {
    name: 'clientName',
    label: 'Cliente',
    field: 'clientName',
    classes: 'bg-white',
    headerClasses: 'bg-grey-2 text-grey-9',
    filter: true,
    sortable: true,
    type: 'string',
    align: 'left',
    copyable: true
  },
  {
    name: 'clientCnpj',
    label: 'CNPJ',
    field: 'clientCnpj',
    classes: 'bg-white',
    headerClasses: 'bg-grey-2 text-grey-9',
    filter: true,
    sortable: true,
    type: 'string',
    align: 'left',
    copyable: true
  },
  {
    name: 'descPrest',
    label: 'Serviço prestado',
    field: 'descPrest',
    classes: 'bg-white',
    headerClasses: 'bg-grey-2 text-grey-9',
    filter: true,
    sortable: true,
    type: 'string',
    align: 'left'
  },
  {
    name: 'descObs',
    label: 'Observação',
    field: 'descObs',
    classes: 'bg-white',
    headerClasses: 'bg-grey-2 text-grey-9',
    filter: true,
    sortable: true,
    type: 'string',
    align: 'left',
    copyable: true
  },
  {
    name: 'location',
    label: 'Local',
    field: value => `${value.location}-${value.uf}`,
    classes: 'bg-white',
    headerClasses: 'bg-grey-2 text-grey-9',
    filter: true,
    sortable: true,
    type: 'string',
    align: 'left',
    copyable: true
  },
  {
    name: 'price',
    label: 'Preço',
    field: 'price',
    classes: 'bg-white',
    headerClasses: 'bg-grey-2 text-grey-9',
    filter: true,
    sortable: true,
    type: 'string',
    align: 'left',
    copyable: true,
    format (value) {
      return parseFloat(value).toFixed(2)
    }
  },
  {
    name: 'dtEmiss',
    label: 'Data de emissão',
    field: 'dtEmiss',
    width: '100px',
    classes: 'bg-white',
    headerClasses: 'bg-grey-2 text-grey-9',
    filter: true,
    sortable: true,
    type: 'date',
    mask: 'YYYY-MM-DD',
    align: 'left',
    format (value) {
      return date.formatDate(value, 'DD/MM/YYYY')
    }
  }
]
export const visibleColumns = ['userLoginPref', 'userPassPref', 'status', 'clientName', 'number', 'clientCnpj', 'descPrest', 'descObs', 'location', 'price', 'dtEmiss']
