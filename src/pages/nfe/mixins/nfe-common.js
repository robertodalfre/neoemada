import { mapActions, mapGetters } from 'vuex'
import { date } from 'quasar'
export const nfeCommon = {
  data: () => ({
    clientOptions: null
  }),
  methods: {
    ...mapActions(['getSubCollection']),
    ...mapGetters(['currentUser']),
    convertStatus (status) {
      switch (status) {
        case 0:
          return 'NFSe - Aguardando Emissão'
        case 1:
          return 'NFSe - Emitida'
        case 2:
          return 'NFSe - Emissão em Andamento'
        case 3:
          return 'NFSe - Cancelada'
        default:
          return 'NFSe - Aguardando Emissão'
      }
    },
    convertColor (status) {
      switch (status) {
        case 0:
          return 'blue-4'
        case 1:
          return 'green-5'
        case 2:
          return 'yellow-9'
        case 3:
          return 'red-5'
        default:
          return 'blue-4'
      }
    },
    convertIcon (status) {
      switch (status) {
        case 0:
          return 'fas fa-stopwatch'
        case 1:
          return 'fas fa-check'
        case 2:
          return 'fas fa-spinner'
        case 3:
          return 'close'
        default:
          return 'fas fa-stopwatch'
      }
    },
    convertDateToText (date) {
      const dt = new Date(date)
      const month = dt.getMonth() + 1
      const day = dt.getDate()
      switch (month) {
        case 1:
          return day + ' JAN'
        case 2:
          return day + ' FEV'
        case 3:
          return day + ' MAR'
        case 4:
          return day + ' ABR'
        case 5:
          return day + ' MAI'
        case 6:
          return day + ' JUN'
        case 7:
          return day + ' JUL'
        case 8:
          return day + ' AGO'
        case 9:
          return day + ' SET'
        case 10:
          return day + ' OUT'
        case 11:
          return day + ' NOV'
        case 12:
          return day + ' DEZ'
      }
    },
    formatPrice (value) {
      const val = (value / 1).toFixed(2).replace('.', ',')
      return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')
    },
    async filterFnUserClients (val, update, abort) {
      if (this.clientOptions !== null) {
        update()
        return
      }
      try {
        const filter = { collection: 'users', collectionId: this.currentUser().userId, subCollection: 'clients' }
        const result = await this.getSubCollection(filter)
        update(() => {
          this.clientOptions = result.map(cur => {
            cur.label = cur.name
            cur.id = cur._id
            return cur
          })
        })
      } catch (e) {
        console.error('err', e)
      }
    },
    convertDate (DataDDMMYY) {
      const format = date.formatDate(DataDDMMYY, 'DD/MM/YYYY')
      const dataSplit = format.split('/')
      const novaData = new Date(
        parseInt(dataSplit[2], 10),
        parseInt(dataSplit[1], 10) - 1,
        parseInt(dataSplit[0], 10))
      return novaData
    }
  }
}
