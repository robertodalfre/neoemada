import services from '@in-house-functions/services'

export const getCompanyDataFromReceita = services.getCompanyDataFromReceita
export const getCity = services.getCity

export default {
  getCompanyDataFromReceita,
  getCity
}
