import { date } from 'quasar'
export const columns = [
  {
    name: 'status',
    label: 'Status',
    field: 'status',
    classes: 'bg-white',
    options: [
      { label: 'Aberto', value: 0, color: 'blue-4' },
      { label: 'Em andamento', value: 1, color: 'yellow-9' },
      { label: 'Concluído', value: 2, color: 'green-5' }
    ],
    mapOptions: {
      0: 'blue-4',
      1: 'yellow-9',
      2: 'green-5'
    },
    mapOptionsIcons: {
      0: 'fas fa-stopwatch',
      1: 'fas fa-spinner',
      2: 'fas fa-check'
    },
    headerClasses: 'bg-grey-2 text-grey-9',
    filter: true,
    sortable: true,
    editable: true,
    type: 'select',
    align: 'left'
  },
  {
    name: 'userEmail',
    label: 'Usuário',
    field: 'userEmail',
    classes: 'bg-white',
    headerClasses: 'bg-grey-2 text-grey-9',
    filter: true,
    sortable: true,
    type: 'string',
    align: 'left'
  },
  {
    name: 'responsibleUserEmail',
    label: 'Responsável',
    field: 'responsibleUserEmail',
    classes: 'bg-white',
    headerClasses: 'bg-grey-2 text-grey-9',
    filter: true,
    sortable: true,
    type: 'string',
    align: 'left'
  },
  {
    name: 'dateTime',
    label: 'Data/Hora',
    field: 'dateTime',
    classes: 'bg-white',
    headerClasses: 'bg-grey-2 text-grey-9',
    filter: true,
    sortable: true,
    type: 'date',
    align: 'left',
    mask: 'YYYY-MM-DD',
    format (value) {
      return date.formatDate(value, 'DD/MM/YYYY HH:mm')
    }
  },
  {
    name: 'type',
    label: 'Tipo',
    field: 'type',
    classes: 'bg-white',
    headerClasses: 'bg-grey-2 text-grey-9',
    filter: true,
    sortable: true,
    options: [
      { label: 'Cliente', value: 'Cliente' },
      { label: 'NFSe', value: 'NFSe' },
      { label: 'Assinatura', value: 'Assinatura' },
      { label: 'Sugestão', value: 'Sugestão' },
      { label: 'Outros', value: 'Outros' }
    ],
    type: 'select',
    align: 'left'
  },
  {
    name: 'lastMessage',
    label: 'Última Mensagem',
    field: value => value.messages[value.messages.length - 1].text,
    classes: 'bg-white',
    headerClasses: 'bg-grey-2 text-grey-9',
    filter: true,
    sortable: true,
    type: 'string',
    align: 'left'
  }
]
export const visibleColumns = ['status', 'userEmail', 'responsibleUserEmail', 'type', 'dateTime', 'lastMessage']
