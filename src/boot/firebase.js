import Firebase from 'firebase/app'
import firebaseConfig from '../../firebase.conf'
import 'firebase/auth'
import 'firebase/firestore'
import 'firebase/messaging'
import 'firebase/functions'

const firebaseApp = Firebase.initializeApp(firebaseConfig)
const firebaseAuth = firebaseApp.auth()
const db = Firebase.firestore()
const functions = Firebase.functions()
const messaging = Firebase.messaging()
messaging.usePublicVapidKey(firebaseConfig.vapidKey)

export { firebaseAuth, db, messaging, Firebase, functions }
