const clearCardNumber = (cardNumber) => {
  return cardNumber.split(' ').join('')
}

const getBrandByCardNumber = (cardNumber) => {
  const number = clearCardNumber(cardNumber)

  if (isNaN(number)) return 'OTHERS'

  const first = number.substr(0, 1)
  const first2 = number.substr(0, 2)
  const first3 = number.substr(0, 3)
  const first4 = number.substr(0, 4)
  const first6 = number.substr(0, 6)

  if (first === '2') {
    if (Number(first6) >= 222100 && Number(first6) <= 272099) return 'MASTERCARD'
  }

  if (first === '3') {
    switch (first2) {
      case '34':
        return 'AMERICAN EXPRESS'
      case '36':
        return 'DINERS CLUB - INTERNATIONAL'
      case '37':
        return 'AMERICAN EXPRESS'
    }

    switch (first3) {
      case '300':
        return 'DINERS CLUB - CARTE BLANCHE'
      case '301':
        return 'DINERS CLUB - CARTE BLANCHE'
      case '302':
        return 'DINERS CLUB - CARTE BLANCHE'
      case '303':
        return 'DINERS CLUB - CARTE BLANCHE'
      case '304':
        return 'DINERS CLUB - CARTE BLANCHE'
      case '305':
        return 'DINERS CLUB - CARTE BLANCHE'
    }

    if (Number(first4) >= 3528 && Number(first4) <= 3589) return 'JCB'
  }

  if (first === '4') {
    switch (first4) {
      case '4026':
        return 'VISA' // VISA ELECTRON
      case '4508':
        return 'VISA' // VISA ELECTRON
      case '4844':
        return 'VISA' // VISA ELECTRON
      case '4913':
        return 'VISA' // VISA ELECTRON
      case '4917':
        return 'VISA' // VISA ELECTRON
    }

    if (first6 === '417500') return 'VISA' // VISA ELECTRON

    return 'VISA' // VISA
  }

  if (first === '5') {
    if (first2 === '54' && number.length === 16) return 'DINERS CLUB - USA & CANADA'

    switch (first4) {
      case '5018':
        return 'MAESTRO'
      case '5020':
        return 'MAESTRO'
      case '5038':
        return 'MAESTRO'
      case '5893':
        return 'MAESTRO'
    }

    switch (first2) {
      case '51':
        return 'MASTERCARD'
      case '52':
        return 'MASTERCARD'
      case '53':
        return 'MASTERCARD'
      case '54':
        return 'MASTERCARD'
      case '55':
        return 'MASTERCARD'
    }
  }

  if (first === '6') {
    switch (first4) {
      case '6304':
        return 'MAESTRO'
      case '6759':
        return 'MAESTRO'
      case '6761':
        return 'MAESTRO'
      case '6762':
        return 'MAESTRO'
      case '6763':
        return 'MAESTRO'
      case '6011':
        return 'DISCOVER'
    }

    switch (first3) {
      case '637':
        return 'INSTAPAYMENT'
      case '638':
        return 'INSTAPAYMENT'
      case '639':
        return 'INSTAPAYMENT'
      case '645':
        return 'DISCOVER'
      case '646':
        return 'DISCOVER'
      case '647':
        return 'DISCOVER'
      case '648':
        return 'DISCOVER'
      case '649':
        return 'DISCOVER'
    }

    if (first2 === '65') return 'DISCOVER'

    if (first3 === 622 &&
      Number(first6) >= 622126 &&
      Number(first6) <= 622925) return 'DISCOVER'
  }

  return 'OTHERS'
}

const getFirstSix = (cardNumber) => {
  const number = clearCardNumber(cardNumber)
  if (isNaN(number)) return '000000'

  return number.substr(0, 6)
}

const getLastFour = (cardNumber) => {
  const number = clearCardNumber(cardNumber)
  if (isNaN(number)) return '0000'

  return number.substr(number.length - 4, 4)
}

const getCardIconByBrand = (brand) => {
  if (brand === 'MASTERCARD' || brand === 'MAESTRO') return 'fab fa-cc-mastercard'
  else if (brand === 'VISA' || brand === 'VISA ELECTRON') return 'fab fa-cc-visa'
  else if (brand === 'DINERS' ||
    brand === 'DINERS CLUB - INTERNATIONAL' ||
    brand === 'DINERS CLUB - CARTE BLANCHE' ||
    brand === 'DINERS CLUB - USA & CANADA'
  ) return 'fab fa-cc-diners-club'
  else if (brand === 'AMEX' || brand === 'AMERICAN EXPRESS') return 'fab fa-cc-amex'
  else if (brand === 'DISCOVER') return 'fab fa-cc-discover'
  return 'fas fa-credit-card'
}

export default {
  getFirstSix: getFirstSix,
  getLastFour: getLastFour,
  getBrandByCardNumber: getBrandByCardNumber,
  getCardIconByBrand: getCardIconByBrand,
  clearCardNumber: clearCardNumber
}
