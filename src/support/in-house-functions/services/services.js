import http from 'src/http'

export const getCep = (cep) => {
  return http.get(`https://viacep.com.br/ws/${cep}/json/`, {})
    .then(response => response.data)
    .catch(err => Promise.reject(new Error(`FAIL_IN_FIND_CEP: ${err}`)))
}

export const getCompanyDataFromReceita = (cnpj) => {
  // console.log('http')
  // console.log(http)
  // return jsonp(`https://www.receitaws.com.br/v1/cnpj/${cnpj}`, { name: 'localhost:8080' })

  return http.get(`https://cors-anywhere.herokuapp.com/https://www.receitaws.com.br/v1/cnpj/${cnpj}`, {
    headers: {
      'X-Final-Url': `https://www.receitaws.com.br/v1/cnpj/${cnpj}`,
      'X-Request-Url': `https://www.receitaws.com.br/v1/cnpj/${cnpj}`
      // 'Access-Control-Request-Method': 'GET'
    }
  })
    .then(response => response)
    .catch(err => Promise.reject(err))
}

export const getCity = (filter) => {
  const { city, state } = filter
  return http.get(`https://br-cidade-estado-nodejs.glitch.me/estados/${state}/cidades?q=${city}`, {})
    .then(response => response)
    .catch(err => Promise.reject(new Error(`FAIL_IN_FIND_CNPJ: ${err}`)))
}
