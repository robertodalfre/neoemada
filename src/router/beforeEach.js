import store from '../store'
const isAuthenticationRoute = route => route.path.indexOf('/auth') !== -1
const isAdminDashboardRoute = route => route.path.indexOf('/dashboard') !== -1

export default async (to, from, next) => {
  if (!isAuthenticationRoute(to)) {
    store().dispatch('heandleAuthStateChanged')
      .then((user) => {
        if (user.emailVerified) {
          if (isAdminDashboardRoute(to) && user.role !== 'admin') {
            next('/links')
          } else {
            next()
          }
        } else {
          next({
            name: 'email-verified.index'
          })
        }
      })
      .catch(() => {
        next('/auth')
      })
  } else {
    next()
  }
}
