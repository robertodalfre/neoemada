
const routes = [
  {

    path: '/',
    meta: { authRequired: true },
    component: () => import('layouts/mainLayout.vue'),
    children: [
      { path: '/links', name: 'links.index', component: () => import('pages/user/components/links.vue') },
      { path: '/clickei', name: 'clickei.index', component: () => import('pages/user/components/clickei.vue') },
      { path: '/clickado', name: 'clickado.index', component: () => import('pages/user/components/clickado.vue') },
      { path: '/clicks', name: 'clicks.index', component: () => import('pages/user/components/clicks.vue') },

      { path: '/detalhe/:client/:nfe', name: 'detail.index', component: () => import('pages/nfe/components/dialogs/dialog-detail-nfe.vue') },
      { path: '/nova', name: 'new.index', component: () => import('pages/nfe/components/dialogs/dialog-new-nfe.vue') },
      { path: '/nova/:client', name: 'new.client.index', component: () => import('pages/nfe/components/dialogs/dialog-new-nfe.vue') },
      { path: '/nova/:client/:nfe', name: 'new.copy', component: () => import('pages/nfe/components/dialogs/dialog-new-nfe.vue') },
      { path: '/clientes', name: 'clients.index', component: () => import('pages/clients/components/clients.vue') },
      { path: '/clientes/buscar', name: 'clients.search', component: () => import('pages/clients/components/dialogs/dialog-find-client.vue') },
      // { path: '/clientes/:id/nova', name: 'clients.nova', component: () => import('pages/nfe/components/dialogs/dialog-new-nfe.vue') },
      // { path: '/config', name: 'config.index', component: () => import('pages/user/components/dialogs/dialog-user-config.vue') },
      // { path: '/mensagens', name: 'messages.index', component: () => import('pages/user/components/dialogs/dialog-user-calls.vue') },
      { path: '/mensagens/nova', name: 'messages.new', component: () => import('components/dialog-chat.vue') },
      { path: '/mensagens/:id', name: 'messages.edit', component: () => import('components/dialog-chat.vue') }
    ]
  },
  // {
  //   path: '/emdesenvolvimento',
  //   component: () => import('layouts/devLayout.vue'),
  //   children: [
  //     { path: '', name: 'emdesenvolvimento.index', component: () => import('pages/emdesenvolvimento.vue') }
  //   ]
  // },
  {
    path: '/auth',
    component: () => import('layouts/devLayout.vue'),
    children: [
      { path: '', name: 'emdesenvolvimento.index', component: () => import('pages/emdesenvolvimento.vue') }
      // { path: 'neoclicks', name: 'neoclicks.index', component: () => import('pages/auth/neoClicks.vue') },
      // { path: 'neoranking', name: 'neoranking.index', component: () => import('pages/auth/neoRanking.vue') },
      // { path: 'reset-password', name: 'reset-password.index', component: () => import('pages/auth/reset-password.vue') },
      // { path: 'register', name: 'register.index', component: () => import('pages/auth/register.vue') },
      // { path: 'email-verified', name: 'email-verified.index', component: () => import('pages/auth/email-verified.vue') }
    ]
  },
  {
    path: '/dashboard',
    meta: { authRequired: true },
    component: () => import('layouts/adminLayout.vue'),
    children: [
      { path: '', name: 'dashboard.index', component: () => import('pages/nfe/components/nfe-main') },
      { path: '/dashboard/nfe', name: 'nfe-admin.index', component: () => import('pages/nfe/components/nfe-main') },
      { path: '/dashboard/chamados', name: 'call-admin.index', component: () => import('pages/call/components/call-main') },
      { path: '/dashboard/chamados/:id', name: 'call-admin.edit', component: () => import('components/dialog-chat.vue') },
      { path: '/dashboard/users', name: 'users-admin.index', component: () => import('pages/user/components/user-main') },
      { path: '/dashboard/users/:id', name: 'user-admin.edit', component: () => import('pages/user/components/user-form') }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
