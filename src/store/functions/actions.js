import { functions } from 'boot/firebase'

export const sendUserNotification = async ({ dispatch }, params) => {
  const { userId, typeMessage, callId } = params
  const sendUserNotification = functions.httpsCallable('sendUserNotification')
  sendUserNotification({ userId, typeMessage, callId })
    .then(result => {
      console.log(result)
    }).catch(e => console.log('ERRO', e))
}

export const sendNotification = async ({ dispatch }, params) => {
  const { userId, typeMessage, callId } = params
  const sendNotification = functions.httpsCallable('sendNotification')
  sendNotification({ userId, typeMessage, callId })
    .then(result => {
      console.log(result)
    }).catch(e => console.log('ERRO', e))
}
