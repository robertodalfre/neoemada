export default {
  MAIN_SET_FETCHING (state, obj) {
    state.fetching = obj.fetching
    state.fetchingMessage = obj.fetchingMessage
  },
  MAIN_SET_MESSAGE (state, obj) {
    state.messages[obj.type] = obj.message
  },
  MAIN_SET_MINISTATE (state, obj) {
    state.miniState = obj.miniState
  }
}
