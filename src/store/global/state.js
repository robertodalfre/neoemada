export default {
  messages: {
    success: '',
    error: [],
    warning: '',
    validation: []
  },
  fetching: false,
  fetchingMessage: '',
  miniState: true
}
