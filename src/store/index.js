import Vue from 'vue'
import Vuex from 'vuex'

import auth from './auth'
import global from './global'
import crud from './crud'
import functions from './functions'
import messaging from './messaging'

export const strict = false

Vue.use(Vuex)

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation
 */

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      auth,
      global,
      crud,
      functions,
      messaging
    },

    // enable strict mode (adds overhead!)
    // for dev mode only
    // strict: process.env.DEV
    strict: false
  })

  return Store
}
