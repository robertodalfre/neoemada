import { db } from 'boot/firebase'

export const getAll = async ({ dispatch }, collection) => {
  const querySnapshot = await db.collection(`${collection}`).get()
  return querySnapshot.docs.map(doc => {
    return {
      _id: doc.id,
      ...doc.data()
    }
  })
}

export const get = async ({ dispatch }, { collection, field, value }) => {
  const querySnapshot = await db.collection(`${collection}`).where(field, '>=', value.toUpperCase()).get()
  return querySnapshot.docs.map(doc => {
    return {
      _id: doc.id,
      ...doc.data()
    }
  })
}

export const getByParameter = async ({ dispatch }, { collection, field, parameter, value }) => {
  const querySnapshot = await db.collection(collection).where(field, parameter, value).get()
  return querySnapshot.docs.map(doc => {
    return {
      _id: doc.id,
      ...doc.data()
    }
  })
}

export const getSubCollection = async ({ dispatch }, params) => {
  const { collection, collectionId, subCollection } = params
  const collectionSelected = db.collection(collection).doc(collectionId).collection(subCollection)
  const querySnapshot = await collectionSelected.get()
  return querySnapshot.docs.map(doc => {
    return {
      _id: doc.id,
      ...doc.data()
    }
  })
}

export const getCollectionByPath = async ({ dispatch }, params) => {
  const { collectionPath } = params
  const collectionSelected = db.doc(collectionPath)
  const doc = await collectionSelected.get()
  if (doc.exists) {
    return {
      _id: doc.id,
      ...doc.data()
    }
  }
}

export const getCollectionByPathOnSnapShot = async ({ dispatch }, params) => {
  const { collectionPath } = params
  return await db.doc(collectionPath)
}

export const getSubCollectionWhere = async ({ dispatch }, params) => {
  const { collection, collectionId, subCollection, filter } = params
  const collectionSelected = db.collection(collection).doc(collectionId).collection(subCollection).where(filter.field, '==', filter.value.toUpperCase())
  const querySnapshot = await collectionSelected.get()
  return querySnapshot.docs.map(doc => {
    return {
      _id: doc.id,
      ...doc.data()
    }
  })
}

export const getCollectionGroup = async ({ dispatch }, params) => {
  const { collectionGroup, filter } = params
  const collectionSelected = filter && filter.field
    ? db.collectionGroup(collectionGroup).where(filter.field, '==', filter.value)
    : db.collectionGroup(collectionGroup)
  const querySnapshot = await collectionSelected.get()
  return querySnapshot.docs.map(doc => {
    return {
      _id: doc.id,
      ...doc.data()
    }
  })
}

export const updateCollectionGroupById = async ({ dispatch }, params) => {
  const { collectionGroup, collectionGroupId, fields } = params
  db.collectionGroup(collectionGroup).doc(collectionGroupId).set(
    JSON.parse(JSON.stringify(fields)))
    .then(docRef => {
      console.log('Document successfully written!')
    })
    .catch(error => {
      dispatch('setUser', {})
      console.error('Error writing document: ', error)
    })
}

export const getById = async ({ dispatch }, params) => {
  try {
    const { collection, id } = params
    if (!collection || !id) return 'Erro de assinatura getById'
    const doc = await db.collection(`${collection}`).doc(id).get()
    if (doc.exists) {
      return {
        _id: doc.id,
        ...doc.data()
      }
    }
    console.log('No such document!')
    return {}
  } catch (e) {
    console.log('Error getting document:', e)
    return e
  }
}

export const updateCollection = async ({ dispatch }, params) => {
  const { collection, collectionId, fields } = params
  if (!collection || !collectionId) return 'Erro de assinatura updateCollection'
  db.collection(`${collection}`).doc(collectionId).update(JSON.parse(JSON.stringify(fields)))
    .then(docRef => {
      console.log('Document successfully written!')
    })
    .catch(error => {
      dispatch('setUser', {})
      console.error('Error writing document: ', error)
    })
}

export const updateById = async ({ dispatch }, params) => {
  const { collection, id, fields } = params
  if (!collection || !id) return 'Erro de assinatura updateById'
  db.collection(`${collection}`).doc(id).set(JSON.parse(JSON.stringify(fields)))
    .then(docRef => {
      console.log('Document successfully written!')
    })
    .catch(error => {
      dispatch('setUser', {})
      console.error('Error writing document: ', error)
    })
}

export const postCreateSubCollection = async ({ dispatch }, params) => {
  const { collection, collectionId, subCollection, fields } = params
  if (!collection || !collectionId || !subCollection || !fields) return 'Erro de assinatura postCreateSubCollection'
  const docRef = await db.collection(collection).doc(collectionId).collection(subCollection).add(JSON.parse(JSON.stringify(fields)))
  console.log('Document successfully written!')
  return docRef.id
}

export const postCreateSubCollection1 = async ({ dispatch }, params) => {
  const { collection, collectionId, subCollection, subCollectionId, subCollection1, fields } = params
  if (!collection || !collectionId || !subCollection || !subCollectionId || !subCollection1 || !fields) return 'Erro de assinatura postCreateSubCollection1'
  db.collection(collection).doc(collectionId).collection(subCollection).doc(subCollectionId).collection(subCollection1)
    .add(JSON.parse(JSON.stringify(fields))).then(docRef => {
      console.log('Document successfully written!')
    })
    .catch(error => {
      console.error('Error writing document: ', error)
    })
}

export const updateSubCollection1 = async ({ dispatch }, params) => {
  const {
    collection,
    collectionId,
    subCollection,
    subCollectionId,
    subCollection1,
    subCollection1Id,
    fields
  } = params
  if (!collection || !collectionId || !subCollection || !subCollectionId || !subCollection1 || !fields) return 'Erro de assinatura updateSubCollection1'
  db.collection(collection).doc(collectionId).collection(subCollection).doc(subCollectionId).collection(subCollection1).doc(subCollection1Id)
    .update({ ...fields }).then(docRef => {
      console.log('Document successfully written!')
    })
    .catch(error => {
      dispatch('setUser', {})
      console.error('Error writing document: ', error)
    })
}

export const updateSubCollection = async ({ dispatch }, params) => {
  const {
    collection,
    collectionId,
    subCollection,
    subCollectionId,
    fields
  } = params
  if (!collection || !collectionId || !subCollection || !subCollectionId || !fields) return 'Erro de assinatura updateSubCollection'
  db.collection(collection).doc(collectionId).collection(subCollection).doc(subCollectionId)
    .update({ ...fields }).then(docRef => {
      console.log('Document successfully written!')
    })
    .catch(error => {
      dispatch('setUser', {})
      console.error('Error writing document: ', error)
    })
}

export const postCreate = async ({ dispatch }, params) => {
  const { collection, fields } = params
  return await db.collection(`${collection}`).add(JSON.parse(JSON.stringify(fields)))
}

export const batchTransaction = async ({ dispatch }, operations) => {
  const { operation1, operation2 } = operations

  const batch = db.batch()

  const subCollection = db.collection(operation1.collection).doc(operation1.collectionId)
    .collection(operation1.subCollection).doc(operation1.subCollectionId)

  const subCollection1 = db.collection(operation2.collection).doc(operation2.collectionId)
    .collection(operation2.subCollection).doc(operation2.subCollectionId)
    .collection(operation2.subCollection1).doc()

  batch.update(subCollection, { ...operation1.fields })
  batch.set(subCollection1, { ...operation2.fields })

  batch.commit().then(function () {
    console.log('Commit transaction successfully')
  })
    .catch(error => {
      console.log('Error commit transaction', error)
    })
}
