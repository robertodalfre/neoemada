import { isEmpty } from 'lodash'
import { messaging, firebaseAuth } from 'boot/firebase'

export const notificationGranted = ({ commit }, granted) => {
  commit('NOTIFICATION_GRANTED', granted)
}

export const sendTokenToServer = async ({ dispatch }, payload) => {
  const { currentToken } = payload
  const user = firebaseAuth.currentUser
  const currentNotificationToken = await dispatch('getCollectionGroup', {
    collectionGroup: 'notificationTokens',
    filter: { field: 'notificationToken', value: currentToken }
  })
  if (isEmpty(currentNotificationToken)) {
    dispatch('postCreateSubCollection', {
      collection: 'users',
      collectionId: user.uid,
      subCollection: 'notificationTokens',
      fields: {
        notificationToken: currentToken,
        userId: user.uid,
        role: user.role
      }
    })
  }
  dispatch('notificationGranted', true)
}

export const messagingRefreshToken = async ({ dispatch }, payload) => {
  messaging.onTokenRefresh(() => {
    messaging.getToken().then((refreshedToken) => {
      console.log('Token refreshed.', refreshedToken)
      // Indicate that the new Instance ID token has not yet been sent to the
      // app server.
      // setTokenSentToServer(false)
      // Send Instance ID token to app server.
      dispatch('sendTokenToServer', { currentToken: refreshedToken })
      // ...
    }).catch((err) => {
      console.log('Unable to retrieve refreshed token ', err)
      // showToken('Unable to retrieve refreshed token ', err)
    })
  })
}

export const messagingRequestPermission = async ({ dispatch }) => {
  try {
    await messaging.requestPermission()
    const token = await messaging.getToken()
    dispatch('sendTokenToServer', { currentToken: token })
  } catch (error) {
    dispatch('notificationGranted', false)
    return error
  }
}
