import { firebaseAuth, db } from 'boot/firebase'
import { date } from 'quasar'

export const setUser = ({ commit }, user) => {
  commit('SET_USER', user)
}

export const setToken = ({ commit }, token) => {
  commit('SET_TOKEN', token)
}

export const signIn = async ({ dispatch }, payload) => {
  const email = payload.email
  const password = payload.password
  await firebaseAuth.signInWithEmailAndPassword(email, password)
    .then(user => {
      console.log('login sucesso')
    })
    .catch(error => {
      console.log(error)
      throw error
    })
}

export const register = async ({ dispatch }, payload) => {
  const email = payload.email
  const password = payload.password
  await firebaseAuth.createUserWithEmailAndPassword(email, password)
    .then(response => {
      const userId = firebaseAuth.currentUser.uid
      dispatch('createUser', { email, userId })
    })
    .catch(error => {
      throw error
    })
}

export const heandleAuthStateChanged = function ({ dispatch }) {
  return new Promise(function (resolve, reject) {
    firebaseAuth.onAuthStateChanged(user => {
      if (user) {
        const user = firebaseAuth.currentUser
        user.getIdToken(/* forceRefresh */ true).then(function (idToken) {
          dispatch('setToken', idToken)
        }).catch(function (error) {
          console.log(error)
          // Handle error
        })
        db.collection('users').doc(user.uid).get().then((doc) => {
          if (doc.exists) {
            const currentUser = doc.data()
            dispatch('setUser', currentUser)
            user.role = currentUser.role
            resolve(user)
          }
        })
      } else {
        dispatch('setUser', {})
        dispatch('setToken', '')
        reject()
      }
    })
  })
}

export const checkIfEmailAlreadyExists = async ({ dispatch }, payload) => {
  try {
    const email = payload.email
    const querySnapshot = await db.collection('users').where('email', '==', email).get()
    return querySnapshot.docs[0] ? querySnapshot.docs[0].id : null
  } catch (error) {
    console.log(error)
    throw error
  }
}

export const sendEmailVerification = async () => {
  await firebaseAuth.currentUser.sendEmailVerification()
    .then(function () {
    }).catch(error => {
      throw error
    })
}

export const sendPasswordResetEmail = async ({ dispatch }, payload) => {
  const emailAddress = payload.email
  try {
    await firebaseAuth.sendPasswordResetEmail(emailAddress)
  } catch (error) {
    return Promise.reject(new Error(error))
  }
}

export const signOut = async ({ dispatch }, payload) => {
  await firebaseAuth.signOut()
    .then(() => {
      dispatch('setUser', {})
      dispatch('setToken', '')
    })
}

export const createUser = ({ dispatch }, params) => {
  const { email, userId } = params
  db.collection('users').doc(userId).set({
    email,
    online: true,
    userId,
    role: 'user',
    status: 0,
    createdAt: `${date.formatDate(Date.now(), 'DD/MM/YYYY')}`
  })
    .then(docRef => {
      console.log('Document successfully written!')
    })
    .catch(error => {
      dispatch('setUser', {})
      console.error('Error writing document: ', error)
    })
}

export const signInGoogle = async ({ dispatch }) => {
  const provider = new firebaseAuth.app.firebase_.auth.GoogleAuthProvider()

  await firebaseAuth.signInWithPopup(provider).then((result) => {
    const userId = firebaseAuth.currentUser.uid
    const email = firebaseAuth.currentUser.email
    db.collection('users').doc(userId).get().then((doc) => {
      if (doc.exists) {
        const currentUser = doc.data()
        dispatch('setUser', currentUser)
        return
      }
      dispatch('createUser', { email, userId })
    })
  }).catch((error) => {
    console.log(error)
  })
}
